<?php

/*
 * bryton-fit-upload
 * Copyright (C) 2020  Stéphane BARON
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Csrf\CsrfToken;

use Bfu\User;
use Bfu\Http;

use GuzzleHttp\Exception\GuzzleException;

require_once __DIR__.'/../vendor/autoload.php';

$cache = new Doctrine\Common\Cache\FilesystemCache(__DIR__.'/../cache/rk');
$app = new Silex\Application();

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../views',
    'twig.options' => array(
        'cache' => __DIR__.'/../cache/twig'
    )
));

$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\CsrfServiceProvider());

// Configuration
$app['config'] = Yaml::parse(file_get_contents(__DIR__ . '/../config/runkeeper.yml'));
$app['debug'] = !isset($_SERVER['SILEX_PROD']);

// Cache permissions
umask(0002);

$checkCallback = function (Request $request) use ($app) {
    if (is_null($request->get('code'))) {
        $app->abort(403, $request->get('error', 'Undefined error'));
    }
    return null;
};

$checkSessionLogged = function () use ($app) {
    if (null === $app['session']->get('runkeeper_session')) {
        return $app->redirect('/login');
    }
    return null;
};

$checkCsrf = function () use ($app) {
    if (!isset($_POST['_csrf_token']) ||
        !$app['csrf.token_manager']->isTokenValid(new CsrfToken('uploadForm', $_POST['_csrf_token']))) {
        $app->abort(403, 'post error');
    }

    return null;
};

/**
 * root controller
 */
$app->get('/', function () use ($app) {
    return $app->redirect('/upload');
})->before($checkSessionLogged);

/**
 * Upload form
 */
$app->get('/upload', function () use ($app, $cache) {
    $user = new User($app['session']->get('runkeeper_session'));

    $client = new Http\Client(
        array(
            'base_uri' => $app['config']['url']['endpoint'],
            'headers' => array(
                'Authorization' => sprintf('%s %s', $user->getTokenType(), $user->getAccessToken()),
                'Accept' => 'application/vnd.com.runkeeper.User+json'
            )
        )
    );

    if (($userInfo = $cache->fetch($user->getCacheTag('user'))) === false) {
        $response = $client->request('GET', '/user');
        $userInfo = json_decode((string)$response->getBody(), true);
        $cache->save($user->getCacheTag('user'), $userInfo, 120);
    }

    if (($userProfile = $cache->fetch($user->getCacheTag('profile'))) === false) {
        $response = $client->request(
            'GET',
            $userInfo['profile'],
            array('headers' => array('Accept' => 'application/vnd.com.runkeeper.Profile+json'))
        );
        $userProfile = json_decode((string)$response->getBody(), true);
        $cache->save($user->getCacheTag('profile'), $userProfile, 60);
    }

    /**
     * @var $csrfTocken CsrfToken
     */
    $csrfTocken = $app['csrf.token_manager']->getToken('uploadForm');

    return $app['twig']->render('upload.twig', array(
        'userProfile' => $userProfile,
        'csrfTocken' => $csrfTocken->getValue()
    ));
})->before($checkSessionLogged);

/**
 * Login page
 */
$app->get('/login', function () use ($app) {
    if ($app['session']->get('runkeeper_session')) {
        return $app->redirect('/upload');
    }

    return $app['twig']->render('login.twig');
});

/**
 * Logout controller
 */
$app->get('/logout', function () use ($app) {
    $app['session']->set('runkeeper_session', null);

    return $app->redirect('/login');
});

/**
 * Activity post controller
 */
$app->post('/upload', function () use ($app, $cache) {
    $user = new User($app['session']->get('runkeeper_session'));

    // Get user infos
    $client = new Http\Client(
        array(
            'base_uri' => $app['config']['url']['endpoint'],
            'headers' => array(
                'Authorization' => sprintf('%s %s', $user->getTokenType(), $user->getAccessToken()),
                'Accept' => 'application/vnd.com.runkeeper.User+json'
            )
        )
    );

    if (($userInfo = $cache->fetch($user->getCacheTag('user'))) === false) {
        $response = $client->request('GET', '/user');
        $userInfo = json_decode((string)$response->getBody(), true);
        $cache->save($user->getCacheTag('user'), $userInfo, 120);
    }

    if (($userProfile = $cache->fetch($user->getCacheTag('profile'))) === false) {
        $response = $client->request(
            'GET',
            $userInfo['profile'],
            array('headers' => array('Accept' => 'application/vnd.com.runkeeper.Profile+json'))
        );
        $userProfile = json_decode((string)$response->getBody(), true);
        $cache->save($user->getCacheTag('profile'), $userProfile, 60);
    }

    $fitFileInfo = isset($_FILES['fitFile']) ? $_FILES['fitFile'] : null;

    // Upload KO
    if (!$fitFileInfo || $fitFileInfo['error'] != 0) {
        return $app['twig']->render(
            'upload-result.twig',
            array(
                'userProfile' => $userProfile,
                'result' => 'error',
                'message' => 'No file uploaded'
            )
        );
    }

    $verboseOutput = isset($_POST['output']) && $_POST['output'] == 'verbose';

    $tmpJsonFile = sprintf('%s/json/%s.json', realpath(__DIR__ . '/../cache'), uniqid('fit', true));

    // Convertion du fichier fit
    $convertCmd = sprintf(
        'php -f %s -- -m json %s %s %s 2>&1',
        escapeshellarg(realpath(__DIR__ . '/../bin/fit-convert.phar')),
        ($verboseOutput) ? '-v' : '',
        escapeshellarg($fitFileInfo['tmp_name']),
        escapeshellarg($tmpJsonFile)
    );

    $cmdOutput = array();
    exec($convertCmd, $cmdOutput, $r);
    $cmdOutputText = join(PHP_EOL, $cmdOutput);

    if ($r !== 0) {
        // Nettoyage
        if (is_file($tmpJsonFile)) {
            unlink($tmpJsonFile);
        }

        return $app['twig']->render(
            'upload-result.twig',
            array(
                'userProfile' => $userProfile,
                'result' => 'error',
                'message' => 'fit-convert error',
                'cmdOutputText' => $cmdOutputText
            )
        );
    }

    // Upload to Runkeeper
    try {
        $response = $client->request(
            'POST',
            $userInfo['fitness_activities'],
            array(
                'headers' => array(
                    'Accept' => '*/*',
                    'Content-Type' => 'application/vnd.com.runkeeper.NewFitnessActivity+json'
                ),
                'body' => file_get_contents($tmpJsonFile)
            )
        );

        $uploadResults = array(
            'userProfile' => $userProfile,
            'result' => ($response->getStatusCode() != 201) ? 'error' : 'success',
            'cmdOutputText' => $cmdOutputText,
            'message' => "Upload status : {$response->getReasonPhrase()} ({$response->getStatusCode()})"
        );
    } catch (GuzzleException $e) {
        $uploadResults = array(
            'userProfile' => $userProfile,
            'result' => 'error',
            'message' => 'upload error',
            'cmdOutputText' => $e->getMessage()
        );
    }

    // Nettoyage
    unlink($tmpJsonFile);

    return $app['twig']->render(
        'upload-result.twig',
        $uploadResults
    );
})->before($checkSessionLogged)->before($checkCsrf);

/**
 * OAuth callback
 */
$app->get('/runkeeper-callback', function (Request $request) use ($app) {
    $client = new Bfu\Http\Client();

    $response = $client->request('POST', $app['config']['url']['token'], array(
        'form_params' => array(
            'grant_type' => 'authorization_code',
            'code' => $request->get('code'),
            'client_id' => $app['config']['client']['id'],
            'client_secret' => $app['config']['client']['secret'],
            'redirect_uri' => $app['config']['url']['callback']
        )
    ));

    if ($response->getStatusCode() != 200) {
        $app->abort(500);
    }

    // access_token OK
    $auth = json_decode((string)$response->getBody(), true);

    // Enregistrement en session
    $app['session']->set('runkeeper_session', $auth);

    // Redirection vers l'index
    return $app->redirect('/');
})->before($checkCallback);


/**
 * Remove app
 */
$app->get('/remove-app', function () use ($app) {
    $user = $app['session']->get('runkeeper_session');

    $client = new Http\Client();

    $client->request('POST', $app['config']['url']['deauthorize'], array(
        'form_param' => array(
            'access_tocken' => $user['access_token']
        )
    ));

    return $app['twig']->render('remove.twig');
})->before($checkSessionLogged);


/**
 * Remove app callback
 */
$app->post('/runkeeper-remove-callback', function () use ($app) {
    return $app->json(array('result' => 'OK'));
});

$app->run();
