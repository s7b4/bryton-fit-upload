const { series, parallel, src, dest } = require('gulp');

/**
 * Plugins
 */
const plugins = require('gulp-load-plugins')();

/**
 * Other plugins
 */
const del = require('del');
const imageminPngquant = require('imagemin-pngquant');

/**
 * Variables
 */
var sourceIcon = "assets/bryton-fit-upload.png";
var nodePath = 'node_modules';

// var awesomePath = nodePath + '/font-awesome';
var bootstrapIconsPath = nodePath + '/bootstrap-icons';

var staticPath = 'web/static';
var staticJsPath = staticPath + '/js';
var staticCssPath = staticPath + '/css';
// var staticFontsPath = staticCssPath + '/fonts';
var staticImagesPath = staticPath + '/images';

function buildImagesApple(cb) {
    return src(sourceIcon)
        .pipe(plugins.gm(function (gmfile) {
              return gmfile.resize(180)
        }))
        .pipe(plugins.rename('apple-touch-icon.png'))
        .pipe(dest('web'));
}

function buildImagesFavicon(cb) {
    return src(sourceIcon)
        .pipe(plugins.gm(function (gmfile) {
              return gmfile.resize(32)
                  .setFormat('ico')
        }, { imageMagick: true })) // gm ne supporte pas les .ico
        .pipe(plugins.rename('favicon.ico'))
        .pipe(dest('web'));
}

function buildImagesMs(cb) {
    return src(sourceIcon)
        .pipe(plugins.gm(function (gmfile) {
              return gmfile.resize(558)
        }))
        .pipe(plugins.rename('tile.png'))
        .pipe(dest('web'));
}

function buildImagesMsWide(cb) {
    return src(sourceIcon)
        .pipe(plugins.gm(function (gmfile) {
              return gmfile.gravity('Center')
                  .resize(270)
                  .extent(558, 270)
                  .background('transparent')
        }))
        .pipe(plugins.rename('tile-wide.png'))
        .pipe(dest('web'));
}

function buildImagesBrand(cb) {
    return src(sourceIcon)
        .pipe(plugins.gm(function (gmfile) {
              return gmfile.resize(30)
        }))
        .pipe(plugins.rename('brand-icon.png'))
        .pipe(dest(staticImagesPath));
}

function optimizeImages(cb) {
    return src(['web/**/*.png'])
        .pipe(plugins.imagemin([imageminPngquant({quality: [0.75, 0.85], strip: true})], {verbose: true}))
        .pipe(dest('web'));
}

function copyFonts(cb) {
    return src(bootstrapIconsPath + '/font/fonts/*.*')
        .pipe(dest(staticFontsPath));
}

function cleanFonts(cb) {
    return del([staticFontsPath]);
}

function buildStaticJs(cb) {
    return src([
        nodePath + '/jquery/dist/jquery.slim.js',
        nodePath + '/popper.js/dist/umd/popper.js',
        nodePath + '/bootstrap/dist/js/bootstrap.js'
    ])
    .pipe(plugins.concat('bryton-fit-upload.js'))
    .pipe(plugins.uglify({compress: {typeofs: false}, mangle: true}))
    .pipe(dest(staticJsPath));
}

function buildStaticCss(cb) {
    return src([
        nodePath + '/bootstrap/dist/css/bootstrap.css'
    ])
    .pipe(plugins.concat('bryton-fit-upload.css'))
    .pipe(plugins.cleanCss({level: 1, format: {breakWith: 'lf'}}))
    .pipe(dest(staticCssPath));
}

exports.build = parallel(
    parallel(
        buildStaticCss,
        buildStaticJs
    ),
    series(
        parallel(
            buildImagesApple,
            buildImagesBrand,
            buildImagesFavicon,
            buildImagesMs,
            buildImagesMsWide
        ),
        optimizeImages
    )
);