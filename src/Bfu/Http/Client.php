<?php

/*
 * bryton-fit-upload
 * Copyright (C) 2020  Stéphane BARON
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace Bfu\Http;

use GuzzleHttp;

class Client extends GuzzleHttp\Client
{
    private static $defaultOptions = array(
        'timeout' => 15.0,
        'connect_timeout' => 5.0,
    );

    private static $defaultHeaders = array(
        'User-Agent' => 'bryton-fit-upload/1.0 PHP/' . PHP_VERSION,
    );

    /**
     * Client constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        // default configuration
        $mergedConfig = array_merge(self::$defaultOptions, $config);

        // headers
        $mergedConfig['headers'] = array_merge(
            self::$defaultHeaders,
            (isset($config['headers'])) ? $config['headers'] : array()
        );

        parent::__construct($mergedConfig);
    }
}
