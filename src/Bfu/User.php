<?php

/*
 * bryton-fit-upload
 * Copyright (C) 2020  Stéphane BARON
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

namespace Bfu;

class User
{
    private $tokenType;
    private $accessToken;

    public function __construct(array $auth)
    {
        // User auth infos
        $this->tokenType = $auth['token_type'];
        $this->accessToken = $auth['access_token'];
    }

    /**
     * @return mixed
     */
    public function getTokenType()
    {
        return $this->tokenType;
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @return string
     */
    private function getTockenHash()
    {
        return md5($this->tokenType . ':' . $this->accessToken);
    }

    public function getCacheTag($id)
    {
        return sprintf('%s-%s', $this->getTockenHash(), $id);
    }
}
