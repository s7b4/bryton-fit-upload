#!/bin/bash

PHAR_URL="https://s3-eu-west-1.amazonaws.com/fit-to-rk/fit-convert.phar"
DEST_FILE="$(dirname $0)/fit-convert.phar"

if [ -x /usr/bin/curl ]; then
    curl --output "${DEST_FILE}" "${PHAR_URL}"
else
    wget --output-document "${DEST_FILE}" "${PHAR_URL}"
fi
